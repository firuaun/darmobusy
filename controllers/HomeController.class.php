<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 19:27
 */

class HomeController {

    public static function main(Request $req, Response $res) {
        $rozklady = Rozklad::find();
        return $res->view(
            'home',
            array('title'=>'darmobusy - Strona główna',
                'rozklady'=>$rozklady
            ),
            'layout'
        );
    }

}