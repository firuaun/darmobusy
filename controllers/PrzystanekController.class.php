<?php
/**
 * User: pharo
 * Date: 2015-01-03
 * Time: 22:42
 */

class PrzystanekController {

    public static function get($req,$res){
        $przystanek_id = $req->param('przystanek');
        $przystanek = Przystanek::findById($przystanek_id);
        return $res->json(PrzystanekHelper::przystanekToArray($przystanek));
    }

    public static function show($req,$res){
        $rozklady = Rozklad::find();
        $przystanek = Przystanek::findById($req->param('id'));
        $trasa = $przystanek->populate('trasa')[0];
        $rozklad = $trasa->populate('rozklad')[0];
        return $res->view('home',array('title'=>'darmobusy - Strona główna',
            'rozklady'=>$rozklady,
            'rozklad'=>$rozklad->id,
            'trasa'=>$trasa->id,
            'przystanek'=>$przystanek->id),'layout');
    }
}