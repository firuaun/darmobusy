<?php
/**
 * User: pharo
 * Date: 2015-01-03
 * Time: 21:37
 */

class TrasaController {

    public static function getFirstStop($req,$res){
        $trasa_id = $req->param('trasa');
        $przystanki = Przystanek::find(array('trasa'=>$trasa_id));
        $last_przystanki_index = count($przystanki)-1;
        if($last_przystanki_index < 0) {
            return $res->json(array('redirect'=>Trasa::findById($trasa_id)->url));
        }
        $przystanek = $przystanki[$last_przystanki_index];
        return $res->json(
            PrzystanekHelper::przystanekToArray($przystanek)
        );
    }

    public static function getPrzystanki($req,$res){
        $trasa_id = $req->param('trasa');
        $przystanki = Przystanek::find(array('trasa'=>$trasa_id));
        return $res->json(
            PrzystanekHelper::przystankiToArray($przystanki)
        );
    }
}