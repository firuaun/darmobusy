<?php
/**
 * User: pharo
 * Date: 2015-01-05
 * Time: 11:37
 */

class MapController {

    public static function getTrasaMap($req,$res){
        $trasa = Trasa::findById($req->param('id'));
        $file_name = sprintf("/routes/%s.gpx",md5($trasa->url));

        return $res->view('map',array('title'=>'darmobusy - '.$trasa->nazwa,'map_title'=>$trasa->nazwa,'file_gpx'=>$file_name,"przystanki"=>array_reverse($trasa->populate("przystanki"))),'layout');
    }

    public static function applyOnMapAndShow($req,$res,$graph,$gpx){
        return $res->view('map',array(
            'title'=>'darmobusy - Wyznaczona trasa',
            'map_title'=>'Wyznaczona trasa',
            'string_gpx'=>str_replace(array("\n","\r\n","\r"),'',$gpx),
            'graph'=>$graph
        ),'layout');
    }
}