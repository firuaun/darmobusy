<?php
/**
 * User: pharo
 * Date: 2015-01-17
 * Time: 21:55
 */

class ApiController {

    public static function rozklady(Request $req, Response $res) {
        $rozklady = $req->hasParam("id") ? Rozklad::find(array('id'=>$req->param("id"))) : Rozklad::find();
        return $res->json(RozkladHelper::rozkladyToArray($rozklady));
    }

    public static function rozkladsTrasy(Request $req, Response $res) {
        $trasy = Trasa::find(array(
            'rozklad'=>$req->param("rozklad")
        ));
        return $res->json(TrasaHelper::trasyToArray($trasy));
    }

    public static function trasy(Request $req, Response $res) {
        $trasy = $req->hasParam("id") ? Trasa::find(array('id'=>$req->param("id"))) : Trasa::find();
        return $res->json(TrasaHelper::trasyToArray($trasy));
    }

    public static function trasasPrzystanki(Request $req, Response $res) {
        $przystanki = Przystanek::find(array(
            'trasa'=>$req->param("trasa")
        ));
        return $res->json(PrzystanekHelper::przystankiToArray($przystanki));
    }

    public static function przystanki(Request $req, Response $res) {
        $przystanki = $req->hasParam("id") ? Przystanek::find(array('id'=>$req->param("id"))) : Przystanek::find();
        return $res->json(PrzystanekHelper::przystankiToArray($przystanki));
    }

    public static function getTrasaMap(Request $req, Response $res){
        $trasa = Trasa::findById($req->param("trasa"));
        $file_path = sprintf("%s/%s.gpx",ROUTES_PATH,md5($trasa->url));
        header ("Content-Type:text/xml");
        return $res->send(file_get_contents($file_path));
    }

    public static function main(Request $req, Response $res){
        return $res->view('api',array('title'=>'darmobusy - Dokumentacja API'),'layout');
    }
}