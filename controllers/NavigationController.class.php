<?php
/**
 * User: pharo
 * Date: 2015-01-05
 * Time: 19:24
 */

class NavigationController {

    public static function determineRoute($req,$res){
        $start = $req->param('start');
        $end = $req->param('end');
        $poczatkowa = NavigationService::translateNameToCoords($start);
        $koncowa = NavigationService::translateNameToCoords($end);

        $graph = NavigationService::determineRouteBetweenAsGraph($poczatkowa,$koncowa,$start,$end);

        return MapController::applyOnMapAndShow($req,$res,$graph,(GeoGraph::graphToGPX($graph)));
    }
}