<?php
/**
 * User: pharo
 * Date: 2015-01-03
 * Time: 20:49
 */

class RozkladController {

    public static function getTrasy($req,$res) {
        $rozklad_id = $req->param('rozklad');
        $trasy = Trasa::find(array("rozklad"=>$rozklad_id));
        $trasy_array = [];
        foreach($trasy as $trasa) {
            $trasy_array[] = array('id'=>$trasa->id,
                'nazwa'=>$trasa->nazwa,
                'url'=>$trasa->url);
        }
        return $res->json($trasy_array);
    }
}