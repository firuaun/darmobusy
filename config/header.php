<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 16:26
 */
define("ROOT_HOST_PATH",'/'.basename(dirname(__DIR__)));
define("ROOT_REAL_PATH",dirname(__DIR__));
define("DIR_SEP",DIRECTORY_SEPARATOR);
define("TMP_DIR_PATH",ROOT_REAL_PATH . DIRECTORY_SEPARATOR . "tmp");
define("ROUTES_PATH" ,sprintf("%s/assets/routes/",ROOT_REAL_PATH));

function classesLoader($class_name){
    autoLoader($class_name,"libraries");
}

function servicesLoader($class_name){
    autoLoader($class_name,"services");
}

function controllersLoader($class_name){
    autoLoader($class_name,"controllers");
}

function modelsLoader($class_name){
    autoLoader($class_name,"models");
}

function viewsLoader($class_name){
    autoLoader($class_name,"views");
}

function autoLoader($class_name,$dir){
    $file = sprintf("%s.class.php",
        ROOT_REAL_PATH . DIR_SEP . $dir . DIR_SEP . $class_name);
    if(file_exists($file)){
        require_once $file;
    }
}

spl_autoload_register('classesLoader');
spl_autoload_register('controllersLoader');
spl_autoload_register('modelsLoader');
spl_autoload_register('viewsLoader');
spl_autoload_register('servicesLoader');