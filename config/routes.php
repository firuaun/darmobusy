<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 16:29
 */

$routes = array_reverse(array(
    '/'=>'HomeController::main',
    '/rozklad/:rozklad'=>'RozkladController::getTrasy',
    '/trasa/:trasa'=>'TrasaController::getFirstStop',
    '/przystanki/:trasa'=>'TrasaController::getPrzystanki',
    '/przystanek/:przystanek'=>'PrzystanekController::get',
    '/map/:id'=>'MapController::getTrasaMap',
    '/show/:id'=>'PrzystanekController::show',
    '/wyznacz'=>'NavigationController::determineRoute',
    '/api'=>'ApiController::main',
    '/api/rozklady'=>'ApiController::rozklady',
    '/api/rozklad/:rozklad/trasy'=>'ApiController::rozkladsTrasy',
    '/api/trasy'=>'ApiController::trasy',
    '/api/trasa/:trasa/przystanki'=>'ApiController::trasasPrzystanki',
    '/api/przystanki'=>'ApiController::przystanki',
    '/api/gpx/:trasa'=>'ApiController::getTrasaMap'
));
