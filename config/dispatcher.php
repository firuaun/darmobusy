<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 15:45
 */
include_once './header.php';
include './routes.php';

$requestedURL = new Url($_SERVER["REQUEST_URI"]);

$route = Route::parse_route($requestedURL->getUrl());
try {
    if(!$route)
        throw new Exception("Nie znaleziono adresu");

    $route->invoke();
}
catch(Exception $ex){
    View::show((new View('responses/internal',array('title'=>'darmobusy - Wewnętrzny błąd','message'=>$ex->getMessage()),'layout')));
}
