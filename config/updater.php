<?php
/**
 * User: pharo
 * Date: 2014-12-28
 * Time: 22:21
 */

include_once 'header.php';

define("UPDATE_TIME_LIMIT",260);

if(!file_exists(TMP_DIR_PATH)){
    mkdir(TMP_DIR_PATH,0755,true);
}
Database::removeDatabase();

Rozklad::make();
Trasa::make();
Przystanek::make();

set_time_limit(UPDATE_TIME_LIMIT);

$start = time();

$rozklady = RegExHelper::regex_get_rozklady(CurlHelper::get(FRIKOBUSY_URL));
foreach($rozklady as  $nazwa_rozkladu => $rozklad) {
    echo sprintf("<strong>%s</strong> (%s)<br/>",$nazwa_rozkladu,$rozklad);
    $save_rozklad = new Rozklad(array(
        "nazwa"=> $nazwa_rozkladu,
        "url"=>$rozklad
    ));
    $trasy = RegExHelper::regex_get_trasy(CurlHelper::get(FRIKOBUSY_URL.$rozklad));
    $save_trasy = [];
    foreach($trasy as $nazwa_linii => $trasa) {
        $trasa_hash = md5($trasa);
        echo sprintf(">><strong>%s</strong> (%s) (hash: %s)<br/>",$nazwa_linii,$trasa,$trasa_hash);
        $save_trasa = new Trasa(array(
            "nazwa"=> $nazwa_linii,
            "url"=>$trasa
        ));
        $linie = RegExHelper::regex_get_przystanki(CurlHelper::get(FRIKOBUSY_URL.$trasa),$trasa);
        $save_linie = [];
        $id_nastepnego = null;
        $ids = [];
        foreach(array_reverse($linie) as $przystanek) {
            echo sprintf(">>><strong>%s</strong>(%s)<br/>",$przystanek[0],$przystanek[1]);
            $tables = RegExHelper::regex_get_tables(CurlHelper::get(FRIKOBUSY_URL.$przystanek[1]));
            $save_przystanek = Przystanek::create(array(
                    "nazwa"=> $przystanek[0],
                    "url"=> $przystanek[1],
                    "timetable"=>serialize($tables["timetable"]),
                    "nexttable"=>serialize($tables["nexttable"])
                )+(is_null($id_nastepnego) ? array() : array("nastepny"=>$id_nastepnego)));
            $id_nastepnego = $save_przystanek->id;
            $ids[] = $id_nastepnego;
            $last_offset = count($save_linie)-1;
            if($last_offset >= 0) {
                $poprzedni = $save_linie[$last_offset];
                $poprzedni->poprzedni = $id_nastepnego;
            }
            $save_linie[] = $save_przystanek;
        }
        $save_trasa->save();
        $save_trasa->populate("przystanki",$save_linie);
        $save_trasy[] = $save_trasa;
        RoutesService::updateWptIds($trasa_hash,array_reverse($ids));
    }
    $save_rozklad->save();
    $save_rozklad->populate("trasy",$save_trasy);
}
echo sprintf("<br/>Whole update took (%d) seconds",time()-$start);