<?php
/**
 * User: pharo
 * Date: 2015-01-03
 * Time: 22:02
 */

class PrzystanekHelper {

    public static function przystanekToArray(Przystanek $przystanek){
        return array(
            'id'=>$przystanek->id,
            'nazwa'=>$przystanek->nazwa,
            'url'=>$przystanek->url,
            'timetable'=>unserialize($przystanek->timetable),
            'nexttable'=>unserialize($przystanek->nexttable),
            'nastepny'=>$przystanek->nastepny,
            'poprzedni'=>$przystanek->poprzedni,
            'trasa'=>$przystanek->trasa
        );
    }

    public static function przystankiToArray($przystanki){
        $przystanki_collection = [];
        foreach(array_reverse($przystanki) as $przystanek){
            $przystanki_collection[] = self::przystanekToArray($przystanek);
        }
        return $przystanki_collection;
    }

}