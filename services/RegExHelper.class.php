<?php
/**
 * User: pharo
 * Date: 2014-12-27
 * Time: 21:05
 */

define("FRIKOBUSY_URL", "http://frikobusy.pl");
define("PRZYSTANEK_TEMPLATE", "/linie/show/id/%d/id_przystanek/%d");

class RegExHelper {

    public static function regex_get_rozklady($return_transfer){
        preg_match_all("/a href=\"(?<url>\/trasy\/.*?)\"\>(?<name>.*?)\s{2,}\<\/a\>/i",$return_transfer,$result);
        $return = array_combine($result["name"],$result["url"]);
        return $return;
    }

    public static function regex_get_trasy($return_transfer){
        preg_match_all("/a href=\"(?<url>\/rozklad\/.*?)\"\>\W*(?<name>.*?)\s{2,}\<\/a\>/i",$return_transfer,$result);
        $return = array_combine($result["name"],$result["url"]);
        return $return;
    }

    public static function regex_get_przystanki($return_transfer,$current_url=null){
        preg_match_all("/option value=\"(?<value>.*?)\"\W*id_linia=\"(?<linia>.*?)\"\W*id_przystanek=\"(?<przystanek>.*?)\"\>\W*?(?<name>.*?)\<\/option\>/i",$return_transfer,$result);
        $names = $result["name"];
        $return = [];
        for($i = 0, $l = count($result[0]); $i < $l; $i++) {
            $value = sprintf(PRZYSTANEK_TEMPLATE,$result["linia"][$i],$result["przystanek"][$i]);
            $return[] = array(trim($names[$i]),$value);
        }
        if(preg_match("/option selected=\"selected\" value=\"(?<value>.*?)\>\W*(?<name>.*?)\<\/option\>/i",$return_transfer,$selected)){
            array_unshift($return,array($selected["name"],$current_url));
        }
        return $return;
    }

    public static function regex_get_tables($return_transfer){
        preg_match_all("/\<span class=\"header2\"\>(?<godziny>[\W\d\<br\/\>]*?)\<\/span\>[\W\s]*?\<\/td>[\W\s]*?<td>[\W\s]*?(?<minuty>.*?)\<\/td\>/i",$return_transfer,$result);
        preg_match_all("/\<span class=\"header\"\>(?<header>.*?)\<\/span\>/i",$return_transfer,$headers);
        preg_match_all("/\<tr\>[\W\s]*?\<td\>[\W\s]*?(?<nastepny>.+?)[\W\s]*?\<\/td\>[\W\s]*?\<td align=\"center\"\>(?<minuty>\d+?)\<\/td\>/i",$return_transfer,$minutes);
        $nexttable = [];
        $i = 0;
        foreach($minutes['nastepny'] as $nastepny) {
            $nexttable[] = array(trim($nastepny),$minutes['minuty'][$i++]);
        }
        $return = [];
        for($i=0,$l=count($result["godziny"]); $i <$l; $i++) {
            $godziny = explode('<br>',preg_replace('/(\s{2,}|\/)/','',$result["godziny"][$i]),-1);
            $minuty = explode('<br>',preg_replace('/(\s{2,}|\/)/','',$result["minuty"][$i]),-1);
            $return[$headers["header"][$i]] = array($godziny,$minuty);
        }
        return array("timetable"=>$return,"nexttable"=>$nexttable);
    }
}