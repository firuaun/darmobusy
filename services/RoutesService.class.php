<?php
/**
 * User: pharo
 * Date: 2015-01-04
 * Time: 18:12
 */

class RoutesService {

    public static function updateWptIds($hash,$ids){
        $file_path = ROUTES_PATH.$hash.".gpx";
        if(!file_exists($file_path)) {
            echo "File doesn't exist<br/>";
            return false;
        }
        $file = new SimpleXMLElement(file_get_contents($file_path));
        $i = 0;
        foreach($file->wpt as $wpt) {
            $wpt->sym = $ids[$i++];
        }
        $file->asXML($file_path);
        echo sprintf("UPDATE FILE %s stats -> count(wpts): %d | count(ids): %d<br/>",
            $file_path,count($file->wpt),count($ids));
    }

}