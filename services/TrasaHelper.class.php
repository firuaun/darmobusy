<?php
/**
 * User: pharo
 * Date: 2015-01-17
 * Time: 22:00
 */

class TrasaHelper {
    public static function trasaToArray(Trasa $trasa){
        return array(
            'id'=>$trasa->id,
            'nazwa'=>$trasa->nazwa,
            'url'=>$trasa->url,
            'rozklad'=>$trasa->rozklad
        );
    }

    public static function trasyToArray(array $trasy){
        $trasa_collection = [];
        foreach($trasy as $trasa){
            $trasa_collection[] = self::trasaToArray($trasa);
        }
        return $trasa_collection;
    }
}