<?php
/**
 * User: pharo
 * Date: 2014-12-27
 * Time: 21:09
 */

class CurlHelper {
    public static function get($url){
        $default_options = array(
            CURLOPT_POST => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 10
        );
        $cl = curl_init();
        curl_setopt_array($cl, $default_options);
        if( ! $result = curl_exec($cl))
        {
            trigger_error(curl_error($cl));
        }
        curl_close($cl);
        return $result;
    }
}