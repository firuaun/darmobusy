<?php
/**
 * User: pharo
 * Date: 2015-01-17
 * Time: 22:00
 */

class RozkladHelper {

    public static function rozkladToArray(Rozklad $rozklad){
        return array(
            'id'=>$rozklad->id,
            'nazwa'=>$rozklad->nazwa,
            'url'=>$rozklad->url
        );
    }

    public static function rozkladyToArray(array $rozklady){
        $rozklad_collection = [];
        foreach($rozklady as $rozklad){
            $rozklad_collection[] = self::rozkladToArray($rozklad);
        }
        return $rozklad_collection;
    }
}