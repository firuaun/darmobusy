<?php
/**
 * User: pharo
 * Date: 2015-01-06
 * Time: 15:07
 */

define("NOMINATIM_QUERY_URL_TEMPLATE","http://nominatim.openstreetmap.org/search?q=%s&format=%s");

class NominatimService {

    public static function findByName($name){
        if(!preg_match('/Łódź/i',$name)) {
            $name = $name." Łódź";
        }
        $url = sprintf(NOMINATIM_QUERY_URL_TEMPLATE,urlencode($name),"xml");
        $xml = simplexml_load_string(CurlHelper::get($url));
        $place = $xml->place;
        return new Coordinates((float)$place['lon'],(float)$place['lat']);
    }

}