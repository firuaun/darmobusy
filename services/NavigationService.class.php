<?php
/**
 * User: pharo
 * Date: 2015-01-05
 * Time: 19:23
 */

include '../libraries/Geo.lib.php';
include '../libraries/Graph.lib.php';
include '../libraries/GeoGraph.lib.php';

class NavigationService {

    public static function translateNameToCoords($name){
        if(preg_match("/\d+?\.\d+?\, \d+?\.\d+/i",$name)) {
            $raw = explode(', ',$name);
            return new Coordinates(floatval($raw[1]),floatval($raw[0]));
        }
        else {
            return NominatimService::findByName($name);
        }
    }

    public static function determineRouteBetweenAsGraph($start_point, $end_point, $start_name=null, $end_name=null){
        $target = new GeoRectangle($start_point,$end_point);
        $start_name = is_null($start_name) ? 'start' : $start_name;
        $end_name = is_null($end_name) ? 'end' : $end_name;
        if($handle = opendir(ROUTES_PATH)) {
            $id = 0;
            $start_node = new GeoNode($id++, $start_point, $start_name, -1, 'wyznaczana');
            $end_node = new GeoNode($id++, $end_point, $end_name, -2, 'wyznaczana');
            $main_edge = array(new DirectedWeightedEdge($start_node, $end_node, FOOT_ROUTE_WEIGHT, FOOT_ROUTE_NAME));
            $graph = new Graph(array($start_node, $end_node), $main_edge);
            while (false !== ($entry = readdir($handle))) {
                if (preg_match('/\.gpx$/i', $entry)) {
                    $file = new SimpleXMLElement(file_get_contents(ROUTES_PATH.$entry));
                    $rectangle = GeoRectangle::find_rectangle($file);
                    if (GeoRectangle::overlap($target, $rectangle)) {
                        //echo sprintf("Match found in %s route.<br/>",$entry);
                        $nodes = [];
                        $edges = [];
                        $prv = null;
                        foreach ($file->wpt as $wpt) {
                            $node = GeoNode::createGeoNodeFromWpt($id++, $wpt, $entry);
                            if (!is_null($prv)) {
                                //connection between bus stops
                                $edges[] = new DirectedWeightedEdge($prv, $node, BUS_ROUTE_WEIGHT, (string)$file->metadata->name);
                            }
                            $nodes[] = $node;
                            $prv = $node;
                        }
                        $first = $nodes[0];
                        $last = $nodes[count($nodes) - 1];
                        //connection between first bus stop and last
                        $edges[] = new DirectedWeightedEdge($first, $last, FOOT_ROUTE_WEIGHT, FOOT_ROUTE_NAME);
                        $edges[] = new DirectedWeightedEdge($last, $first, FOOT_ROUTE_WEIGHT, FOOT_ROUTE_NAME);
                        //merging graphs (routes)
                        $graph = is_null($graph) ? new Graph($nodes, $edges) : GeoGraph::connectGraphsWithDirectedEdges($graph, new Graph($nodes, $edges), FOOT_ROUTE_WEIGHT, FOOT_ROUTE_NAME);
                        //echo sprintf("Nodes: %d<br/>Edges: %d<br/>",count($nodes),count($edges));
                    }
                }
            }
            $path = (new GeoDijkstra($graph, $start_node))->find_shortes_path($end_node);
            array_unshift($path[0], $start_node);
            closedir($handle);
            return new Graph($path[0],$path[1]);
        }
    }
}