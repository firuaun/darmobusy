<?php
/**
 * User: pharo
 * Date: 2014-12-28
 * Time: 22:13
 */

class Rozklad extends Model {

    static public $mapping = array(
        "trasy"=>array(
            "collection"=>"Trasa",
            "via"=>"rozklad"
        )
    );

    public function __construct($initialize = null){
        parent::__construct();
        if(!is_null($initialize)) {
            $this->attributes = $initialize;
        }
    }

    public static function make(){
        $createQuery = <<<EOSQL
CREATE TABLE Rozklad(
	id INTEGER PRIMARY KEY,
	nazwa TEXT DEFAULT NULL,
	url TEXT DEFAULT NULL
)
EOSQL;
        return Model::$db->exec($createQuery);
    }

}