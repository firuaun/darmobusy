<?php
/**
 * User: pharo
 * Date: 2014-12-28
 * Time: 22:12
 */

class Przystanek extends Model {

    static public $mapping = array(
        "trasa"=>array(
            "model"=>"Trasa"
        ),
        "nastepny"=>array(
            "model"=>"Przystanek",
            "via"=>"poprzedni"
        ),
        "poprzedni"=>array(
            "model"=>"Przystanek",
            "via"=>"nastepny"
        )
    );

    public function __construct($initialize = null){
        parent::__construct();
        if(!is_null($initialize)) {
            $this->attributes = $initialize;
        }
    }

    public static function make(){
        $createQuery = <<<EOSQL
CREATE TABLE Przystanek(
	id INTEGER PRIMARY KEY,
	nazwa TEXT DEFAULT NULL,
	url TEXT DEFAULT NULL,
	timetable BLOB DEFAULT NULL,
	nexttable BLOB DEFAULT NULL,
	nastepny INTEGER DEFAULT NULL,
	poprzedni INTEGER DEFAULT NULL,
	trasa INTEGER DEFAULT NULL,
	FOREIGN KEY(trasa) REFERENCES Trasa(id),
	FOREIGN KEY(nastepny) REFERENCES Przystanek(id),
	FOREIGN KEY(poprzedni) REFERENCES Przystanek(id)
)
EOSQL;
        return Model::$db->exec($createQuery);
    }

}