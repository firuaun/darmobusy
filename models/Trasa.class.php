<?php
/**
 * User: pharo
 * Date: 2014-12-28
 * Time: 22:12
 */

class Trasa extends Model {

    static public $mapping = array(
        "rozklad"=>array(
            "model"=>"Rozklad"
        ),
        "przystanki"=>array(
            "collection"=>"Przystanek",
            "via"=>"trasa"
        )
    );

    public function __construct($initialize = null){
        parent::__construct();
        if(!is_null($initialize)) {
            $this->attributes = $initialize;
        }
    }

    public static function make(){
        $createQuery = <<<EOSQL
CREATE TABLE Trasa(
	id INTEGER PRIMARY KEY,
	nazwa TEXT DEFAULT NULL,
	url TEXT DEFAULT NULL,
	rozklad INTEGER DEFAULT NULL,
	FOREIGN KEY(rozklad) REFERENCES Rozklad(id)
)
EOSQL;
        return Model::$db->exec($createQuery);
    }

}