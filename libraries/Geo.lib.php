<?php

define("EARTH_R", 6371);

class Coordinates {
	public $lat; //Y
	public $lon; //X
	public function __construct($lon,$lat){
		$this->lat = $lat;
		$this->lon = $lon;
	}
	
	public function swap(){
		$tmp = $this->lat;
		$this->lat = $this->lon;
		$this->lon = $tmp;
		return $this;
	}
	private static function convertDegreesToRadians($degrees){
		return M_PI * $degrees / 180;
	}

	public static function measureDistanceBetween(Coordinates $p1, Coordinates $p2){
		$rad_lat1 = self::convertDegreesToRadians($p1->lat);
		$rad_lat2 = self::convertDegreesToRadians($p2->lat);
		$rad_lon1 = self::convertDegreesToRadians($p1->lon);
		$rad_lon2 = self::convertDegreesToRadians($p2->lon);

		$x = ($rad_lon2-$rad_lon1)*cos(($rad_lat1+$rad_lat2)/2);
		$y = $rad_lat2-$rad_lat1;

		return round(EARTH_R * sqrt(pow($x,2)+pow($y,2)) * 1000);
	}
}

class GeoRectangle {
	/*
		(max->lon, max->lat) ------ (min->lon, max->lat)
		|											   |
		|											   |
		|											   |
		(max->lon, min->lat) ------ (min->lon, min->lat)
	*/
	public $maxPoint;
	public $minPoint;
	public function __construct(Coordinates $point1, Coordinates $point2){
		if($point1->lon < $point2->lon){
			$maxX = $point1->lon;
			$minX = $point2->lon;
		}
		else {
			$maxX = $point2->lon;
			$minX = $point1->lon;
		}
		if($point1->lat > $point2->lat){
			$maxY = $point1->lat;
			$minY = $point2->lat;
		}
		else {
			$maxY = $point2->lat;
			$minY = $point1->lat;
		}
		$this->maxPoint = new Coordinates($maxX,$maxY);
		$this->minPoint = new Coordinates($minX,$minY);
	}
	
	public static function overlap(GeoRectangle $x1, GeoRectangle $x2){
		if($x2->maxPoint->lon > $x1->minPoint->lon || $x2->minPoint->lon < $x1->maxPoint->lon){
			return false;
		}
		if($x2->maxPoint->lat < $x1->minPoint->lat || $x2->minPoint->lat > $x1->maxPoint->lat){
			return false;
		}
		return true;
	}

	public static function find_rectangle($file){
		$boundary = array(array((float)$file->wpt[0]['lat'],(float)$file->wpt[0]['lon']),array((float)$file->wpt[0]['lat'],(float)$file->wpt[0]['lon']));
		foreach($file->wpt as $waypoint){
			$lat = (float) $waypoint['lat'];
			$lon = (float) $waypoint['lon'];
			if($boundary[0][0] < $lat){
				$boundary[0][0] = $lat;
			}
			if($boundary[0][1] > $lon){
				$boundary[0][1] = $lon;
			}
			if($boundary[1][0] > $lat){
				$boundary[1][0] = $lat;
			}
			if($boundary[1][1] < $lon){
				$boundary[1][1] = $lon;
			}
		}
		return new GeoRectangle(new Coordinates($boundary[0][1],$boundary[0][0]),new Coordinates($boundary[1][1],$boundary[1][0]));
	}
}