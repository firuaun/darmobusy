<?php

class PriorityQueue extends SplPriorityQueue {

	protected $reverseOrder = PHP_INT_MAX;
	
	public function insert($data,$priority){
		if(is_numeric($priority)){
			$priority = $this->reverseOrder-$priority;
		}
		parent::insert($data,$priority);
	}
}