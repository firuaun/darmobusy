<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:48
 */

class QueryUtils {
    const INSERT_STATEMENT = "INSERT INTO %s %s VALUES %s";
    const UPDATE_STATEMENT = "UPDATE %s SET %s WHERE %s";

    public static function array_key_value_to_string($array,$pair_separator='=',$between_separator=', ', $key_quote = '`', $value_quote = "'"){
        $statement = array();
        foreach($array as $key => $value) {
            $statement[] = sprintf("%s%s%s%s%s%s%s",$key_quote,$key,$key_quote,
                $pair_separator,
                $value_quote,$value,$value_quote);
        }
        return implode($between_separator,$statement);
    }

    public static function array_value_to_string($array,$value_quote="'",$between_separator=', ',$parenthesis="()"){
        $statement = array();
        foreach($array as $value){
            $statement[] = sprintf("%s%s%s",$value_quote,$value,$value_quote);
        }
        return $parenthesis[0].implode($between_separator,$statement).$parenthesis[1];
    }

    public static function prepare_insert_statement($into, $key_set){
        return sprintf(QueryUtils::INSERT_STATEMENT,
            $into,
            QueryUtils::array_value_to_string($key_set,''),
            QueryUtils::array_value_to_string(array_fill(0,count($key_set),'?'),'',', ')
        );
    }

    public static function prepare_update_statement($into, $key_set, $where) {
        return sprintf(QueryUtils::UPDATE_STATEMENT,
            $into,
            QueryUtils::array_key_value_to_string(array_fill_keys($key_set,'?'),'=',', ','',''),
            $where
        );
    }
}