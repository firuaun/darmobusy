<?php

define("UNDEFINED","undefined");

include_once 'PriorityQueue.class.php';

class Graph {
	private $nodes;
	private $edges;
	public function __construct($nodes, $edges = null){
		$this->nodes = $nodes;
		$this->edges = $edges;
	}
	
	public function get_node_by_id($id){
		$found = null;
		foreach($this->nodes as $node){
			if($node->get_id() === $id) {
				$found = $node;
				break;
			}
		}
		return $found;
	}
	public function get_nodes(){
		return $this->nodes;
	}
	public function get_edges(){
		return $this->edges;
	}
}

class Node {
	private $edges = [];
	private $scanned = false;
	private $id;
	public function __construct($id){
		$this->id = $id;
	}
	public function add_edge(Edge $e){
		$this->edges[] = $e;
		return $e;
	}
	public function get_edges(){
		return $this->edges;
	}
	public function get_neightbours(){
		$n = [];
		foreach($this->edges as $edge){
			$n[] = $edge->get_other_node($this);
		}
		return $n;
	}
	public function get_id(){
		return $this->id;
	}
	public function get_connected_to_nodes_and_edge(){
		$n = [];
		foreach($this->edges as $edge){
			if($edge instanceof DirectedEdge && $this !== ($tmp = $edge->get_to($this))) {
				$n[] = array($tmp,$edge);	
			}
		}
		return $n;
	}
	public function mark($mark = null){
		if(!is_null($mark))
			$this->scanned = $mark;
		return $this->scanned;
	}
}

class Edge {
	protected $nodes;
	public function __construct(Node $a, Node $b){
		$this->nodes = array($a,$b);
		$a->add_edge($this);
		$b->add_edge($this);
	}
	public function get_nodes(){
		return $this->nodes;
	}
	public function get_other_node($a){
		return $node[0] === $a ? $b : $a;
	}
}

class DirectedEdge extends Edge {
	public function __construct(Node $a, Node $b){
			parent::__construct($a, $b);
	}
	public function get_from(){
		return $this->nodes[0];
	}
	public function get_to(){
		return $this->nodes[1];
	}
	public function set_from(Node $a){
		return ($this->nodes[0] = $a);
	}
	public function set_to(Node $b){
		return ($this->nodes[0] = $a);
	}
}

class DirectedWeightedEdge extends DirectedEdge {
	private $weight;
	private $name;
	public function __construct(Node $from, Node $to, $weight,$name = null){
		parent::__construct($from, $to);
		$this->weight = $weight;
		$this->name = $name;
	}
	public function get_name(){
		return $this->name;
	}
	public function get_weight(){
		return $this->weight;
	}
	public function set_weight($weight){
		return ($this->weight = $weight);
	}
}

class Dijkstra {
	private $d = [];
	private $p = [];
	private $pE = [];
	private $Q;
	private $G;
	private $s;
	public function __construct(Graph $graph, Node $source){
		$this->G = $graph;
		$this->s = $source;
		$this->Q = new PriorityQueue();
	}
	
	public function find_shortes_path($to=null){
		$this->dist($this->s,0);
		foreach($this->G->get_nodes() as $v){
			if($v !== $this->s) {
				$this->dist($v,INF);
				$this->prev($v,UNDEFINED);
			}
			$this->add_node_to_Q($v);
		}
		//usort($this->Q,array($this,"sort_Q"));
		while(!$this->Q->isEmpty()){
			$u = $this->get_min_node_from_Q();
			$u->mark(true);
			if(!is_null($to) && $to === $u){
				$sequence = [];
				$edgeSequence = [];
				while(!is_null(($prev = $this->prev($u)))) {
					array_unshift($sequence,$u);
					$u = $prev;
				}
				$u = $to;
				while(!is_null(($prev = $this->prevEdge($u)))){
					array_unshift($edgeSequence,$prev);
					$u = $prev->get_from();
				}
				return array($sequence,$edgeSequence);
			}
			//echo sprintf("Min Q pop: %s | Distance: %f<br/>",$u->get_id(),$this->dist($u));
			foreach($u->get_connected_to_nodes_and_edge() as $connection) {
				$edge = $connection[1];
				$v = $connection[0];
				if(!$v->mark()) {
					$alt = ($this->dist($u) == INF ? 0 :$this->dist($u)) + $this->length($edge);
					//echo sprintf("Neighbour: %s | Weight: %d | dist: %f |alt: %f<br/>",$v->get_id(),$edge->get_weight(),$this->dist($v),$alt);
					if( $alt < $this->dist($v) ) {
						$this->dist($v, $alt);
						$this->prev($v, $u);
						$this->prevEdge($v,$edge);
						$this->add_node_to_Q($v);
						//usort($this->Q,array($this,"sort_Q"));
					}
				}
			}
		}
		return array($this->d,$this->p);
		
	}
	
	private function dist(Node $n, $value = null){
		if(!is_null($value)){
			return ($this->d[$n->get_id()]=$value);
		}
		return $this->d[$n->get_id()];
	}
	
	private function prev(Node $n, $value = null){
		if(!is_null($value)){
			return ($this->p[$n->get_id()]=$value);
		}
		return array_key_exists($n->get_id(),$this->p) ? $this->p[$n->get_id()]:null;
	}
	
	private function prevEdge(Node $n, $value = null){
		if(!is_null($value)){
			return ($this->pE[$n->get_id()]=$value);
		}
		return array_key_exists($n->get_id(),$this->pE) ? $this->pE[$n->get_id()]:null;
	}
	
	private function add_node_to_Q(Node $node){
		$this->Q->insert($node,$this->dist($node));
		//$this->Q[$node->get_id()] = array($this->dist($node),$node);
	}
	
	private function get_min_node_from_Q(){
		return $this->Q->extract();
		//return array_shift($this->Q)[1];
	}
	
	/* private function update_node_in_Q(Node $node){
		$this->Q[] = array($this->dist($node),$node);
	} */
	
	/* private function sort_Q($a,$b){
		if ($a[0] == $b[0]) {
			return 0;
		}
		return ($a[0] < $b[0]) ? -1 : 1;
	} */
	
	protected function length(Edge $edge){
		return $edge->get_weight();
	}
}
