<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:48
 */

define("DATABASE_TYPE","sqlite");
define("DATABASE_HOST",TMP_DIR_PATH . DIR_SEP . "memory.sqlite");

class Database {
    private static $db;

    static function getInstance(){
        if(is_null(Database::$db)){
            Database::$db = new PDO(sprintf("%s:%s",DATABASE_TYPE,DATABASE_HOST));
        }
        return Database::$db;
    }

    static function removeDatabase(){
        if(file_exists(DATABASE_HOST)){
            unlink(DATABASE_HOST);
        }
    }

}