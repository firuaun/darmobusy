<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:47
 */

abstract class Model {

    static public $db;
    static public $mapping;
    protected $attributes;
    protected $isPersisted = false;

    public function __construct(){
        if(isset($this->attributes["id"])){
            $this->isPersisted = true;
        }
    }

    public static function init(){
        self::$db = Database::getInstance();
    }

    public static function findById($id){
        $class = get_called_class();
        return $class::find(array("id"=>$id),$class)[0];
    }
    public static function find(array $conditions = null, $class = null) {
        $class = is_null($class) ? get_called_class() : $class;
        $whereStatement = is_null($conditions) ? '' : sprintf(" WHERE %s",QueryUtils::array_key_value_to_string($conditions,'=',' AND ', '',"'"));
        $statement = sprintf("SELECT * FROM %s%s",
            $class,
            $whereStatement);
        return Database::getInstance()->query($statement)->fetchAll(PDO::FETCH_CLASS,$class);
    }

    abstract static public function make();

    public static function create(array $config){
        $class = get_called_class();
        $object = new $class($config);
        return $object->save();
    }

    public function save(){
        return $this->isPersisted ? $this->update() : $this->insert();
    }
    public function delete($id){
        echo "delete method under construction";
    }

    public function insert(){
        $query = self::$db->prepare(QueryUtils::prepare_insert_statement(get_class($this),array_keys($this->attributes)));
        $insertResult = $query->execute(array_values($this->attributes));
        if($insertResult) {
            $this->id = self::$db->lastInsertId();
            $this->isPersisted = true;
            return $this;
        }
        return $insertResult;
    }
    public function update(){
        $query = self::$db->prepare(QueryUtils::prepare_update_statement(get_class($this),array_keys($this->attributes),sprintf("id=%d",$this->id)));
        $updateResult = $query->execute(array_values($this->attributes));
        if($updateResult) {
            return $this;
        }
        return $updateResult;
    }


    public function __set($name,$value){
        $this->attributes[$name] = $value;
    }

    public function __get($name){
        return $this->attributes[$name];
    }

    public function populate($name,$value=null){
        $populated = $this;
        $class = get_called_class();
        if(array_key_exists($name,$class::$mapping)) {
            $key = $class::$mapping[$name];
            if(array_key_exists("collection",$key)) {
                //one-to-many
                $model = $key["collection"];
                $via = $key["via"];
                if(!is_null($value)) {
                    if(is_array($value)){
                        foreach($value as $row){
                            $row->$via = $this->id;
                            $row->save();
                        }
                    }
                    else {
                        $value->$via = $this->id;
                        $value->save();
                    }
                    $populated = $value;
                }
                else {
                    $populated = $model::find(array($via=>$this->id));
                    $this->$name = $populated;
                }
            }
            else if(array_key_exists("model",$key)){
                //many-to-one or one-to-one or one-side
                $model = $key["model"];
                if(!is_null($value)) {
                    if(array_key_exists("via",$key)){
                        //one-to-one
                        $via = $key["via"];
                        $value->$via = $this->id;
                        $value->save();
                        $populated = $value;
                    }
                    $this->$name = $value->id;
                    $this->save();
                }
                else {
                    $model_id = $this->$name;
                    $populated = $model::find(array("id"=>$model_id));
                    $this->$name = $populated[0];
                }
            }
        }
        return $populated;
    }
}

Model::init();