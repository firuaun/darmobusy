<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 16:36
 */

class Url {
    private $url;

    public function __construct($url){
        $this->url = $this->cleanUrl($url);
    }

    private function cleanUrl($url) {
        return str_replace(ROOT_HOST_PATH,'',$url);
    }

    public function getUrl(){
        return $this->url;
    }

    public function setUrl($url){
        $this->url = $url;
    }
}