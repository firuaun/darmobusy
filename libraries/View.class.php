<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 18:42
 */

class View {
    private $viewName;
    private $locals;
    private $layout;

    public function __construct($viewName,$locals=null,$layout=null){
        $this->viewName = $viewName;
        $this->locals = $locals;
        $this->layout = $layout;
    }

    public function render(){
        ob_start();
        extract($this->locals);
        $view = $this->viewName;
        $body = null;
        if(!is_null($this->layout)) {
            $view = $this->layout;
            $body = $this->nestedView();
        }
        include sprintf("%sviews%s.html",
            ROOT_REAL_PATH . DIR_SEP,
            DIR_SEP . $view);
        return ob_get_clean();
    }

    public static function show($view){
        echo $view->render();
    }

    public function nestedView(){
        $this->layout = null;
        return $this;
    }

    public function partial($path){
        return View::show(new View($path,$this->locals));
    }

    public static function asset($asset){
        return ROOT_HOST_PATH.'/assets'.$asset;
    }
}